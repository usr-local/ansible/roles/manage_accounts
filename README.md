manage_accounts
=========

Role used to manage local accounts (be them regular or system accounts) as well
as various bits and pieces related to them:
- creation of groups
- configuration of public & private SSH keys

Requirements
------------

N/A

Role Variables
--------------

The role reads a couple of lists (but can be extended to using others if need
be).  There are several as to cleanly separate at least regular accounts from
system accounts.

`.0.*` entries are examples of the first item in the aforementioned list.

The two lists that get exactly the same parameters are:
- accounts
- system_accounts

We're explaining `accounts` below:

| Variable                   | Type     | Mandatory | Description                                                                     |
|----------------------------|----------|-----------|---------------------------------------------------------------------------------|
| `accounts`                 | `list`   | `false`   | List storing entries of groups with (potentially) associated accounts           |
| `.0.group`                 | `string` | `true`    | Name of group storing one or several accounts                                   |
| `.0.gid`                   | `number` | `false`   | Use a specific GID for the group                                                |
| `.0.users`                 | `list`   | `false`   | List storing users that belong to this group                                    |
| `.0.users.0.name`          | `string` | `true`    | Username for the account                                                        |
| `.0.users.0.system`        | `bool`   | `false`   | Whether this is a system account or not                                         |
| `.0.users.0.uid`           | `string` | `false`   | Use a specific UID for the username                                             |
| `.0.users.0.groups`        | `list`   | `false`   | List of additional groups the user might be a part of                           |
| `0.users.0.password`       | `string` | `false`   | Hash password for the account                                                   |
| `0.users.0.home`           | `string` | `false`   | Specify a custom home directory for the account                                 |
| `0.users.0.pubkey`         | `string` | `false`   | Public keys (multi-line YAML) associated with this account; managed exclusively |
| `0.users.0.privkey`        | `list`   | `false`   | List storing private keys that belong to this username                          |
| `0.users.0.privkey.0.name` | `string` | `false`   | Basename for the provided private key e.g., `$HOME/.ssh/<name>`                 |
| `0.users.0.privkey.0.pkey` | `string` | `false`   | Actual private key or reference to vault-encrypted var                          |

### Examples ###

    accounts:
      - group: Flintstones
        users:
          - name: Fred
            pubkey: |
              ssh-rsa fred@home
              ssh-rsa fred@bowling-alley
          - name: Wilma
            uid: 9690
            pubkey: |
              ssh-rsa wilma@home
            privkey:
              - name: home-key
                pkey: "{{ vault_wilma_home_pkey }}"

This would create the `Flintstones` group with `Fred` and `Wilma` as users,
with `Wilma` being assigned UID `9690`.
`Fred` gets two public keys in his `~/.ssh/authorized_keys` file, while Wilma
gets a single public key in her `~/.ssh/authorized_keys` file but also gets a
private key under `~/.ssh/home-key`.

Dependencies
------------

N/A

Example Playbook
----------------

Suppose one uses a `bedrock.ini` inventory file that contains these hosts:

    [all]
    bedroom-pc.bedrock.lan
    living-pc.bedrock.lan

and a `manage_accounts.yml` playbook that looks like this:

    - hosts: all
      roles:
         - role: manage_accounts

one could call ansible like this:

    $ ansible-playbook -i bedrock.ini manage_accounts.yml

License
-------

BSD

Author Information
------------------

[psa90](https://gitlab.com/psa90)

